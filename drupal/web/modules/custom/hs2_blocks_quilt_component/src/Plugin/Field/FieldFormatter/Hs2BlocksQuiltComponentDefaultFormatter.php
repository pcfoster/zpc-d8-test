<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component\Plugin\Field\FieldFormatter\Hs2BlocksQuiltComponentDefaultFormatter.
 */

namespace Drupal\hs2_blocks_quilt_component\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\hs2_blocks_quilt_component\ProcessAtom;
use Drupal\hs2_blocks_quilt_component\ProcessStyles;

/**
 * Plugin implementation of the 'hs2_blocks_quilt_component_default' formatter.
 *
 * @FieldFormatter(
 *   id = "hs2_blocks_quilt_component_default",
 *   label = @Translation("HS2 Quilt Side"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class Hs2BlocksQuiltComponentDefaultFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The atom processor service.
   *
   * @var \Drupal\hs2_blocks_quilt_component\ProcessAtom
   */
  protected $atomProcessor;

  /**
   * The styles processor service.
   *
   * @var \Drupal\hs2_blocks_quilt_component\ProcessStyles
   */
  protected $stylesProcessor;

  /**
   * Constructs a Hs2BlocksQuiltComponentDefaultFormatter instance.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ProcessAtom $atom_processor, ProcessStyles $styles_processor) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->atomProcessor = $atom_processor;
    $this->stylesProcessor = $styles_processor;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('hs2_blocks_quilt_component.process_atom_service'),
      $container->get('hs2_blocks_quilt_component.process_styles_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $summary[] = t('Displays a Quilt Side (Left/Right).');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    // Get the block entity containing the paragraph entity reference, which
    // contains the layout governing display of the paragraphs.
    $parent_entity = $items->getEntity();

    foreach ($items as $delta => $item) {
      // Determine which side the atom belongs to.
      $item_parent = $item->getParent()->getName();

      // Get the actual paragraph entity containing field values.
      $paragraph_ref = $item->get('target_id')->getValue();
      if ($paragraph = \Drupal::entityTypeManager()->getStorage('paragraph')->load($paragraph_ref)) {

        // Get the selected layout for that side, defaulting to a fallback
        // template if no layout option is available.
        $layout_field = str_replace('side', 'layout', $item_parent);
        $layout = isset($parent_entity->{$layout_field}->value) ? $parent_entity->{$layout_field}->value : 'hs2_quilt_default';
        $elements[0]['#theme'] = $layout;

        // Get all non-layout classes for applied styling.
        $elements[0]['#article_classes'][$delta] = $this->stylesProcessor->buildAtomClasses($paragraph);

        // Process the atom according to type, normalizing all values.
        $elements[0]['#content'][$delta] = $this->atomProcessor->buildAtomValues($paragraph, $layout, $delta);
      }
    }

    return $elements;
  }
}