<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component\GetImageStyleInterface.
 */

namespace Drupal\hs2_blocks_quilt_component;

interface GetImageStyleInterface {

  /**
   * Return an image style based on the selected layout and the position of the
   * current atom within that layout.
   *
   * @param $layout
   *  The selected layout for the quilt side containing the atom.
   * @param $delta
   *  The delta of the atom indicating its order in the containing paragraph.
   *
   * @return string
   *  The machine name of an image style.
   */
  public function getAtomImageStyle($layout, $delta);
}