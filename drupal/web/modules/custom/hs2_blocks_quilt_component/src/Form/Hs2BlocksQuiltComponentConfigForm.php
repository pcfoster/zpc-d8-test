<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component\Form\Hs2BlocksQuiltComponentConfigForm.
 */

namespace Drupal\hs2_blocks_quilt_component\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class Hs2BlocksQuiltComponentConfigForm.
 *
 * @package Drupal\hs2_blocks_quilt_component\Form
 */
class Hs2BlocksQuiltComponentConfigForm extends ConfigFormBase {

  /**
   * Default layouts provided by the quilt module.
   */
  private $defaultQuiltLayouts;

  /**
   * Constructs a Hs2BlocksQuiltComponentConfigForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->setConfigFactory($config_factory);
    $this->defaultQuiltLayouts = 'hs2_quilt_one_2x2|One 2x2
hs2_quilt_two_2x1|Two 2x1
hs2_quilt_two_1x2|Two 1x2
hs2_quilt_four_1x1|Four 1x1
hs2_quilt_two_1x1_one_2x1|Two 1x1 & One 2x1
hs2_quilt_one_2x1_two_1x1|One 2x1 & Two 1x1';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hs2_blocks_quilt_component.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hs2_blocks_quilt_component_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hs2_blocks_quilt_component.settings');

    // Layout options & Twig template identification.
    $form['layouts'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Quilt Side Layouts'),
      '#description' => $this->t('Customize the available layouts for the quilt sides by adding new options here.<br />
        Each line should be a key|value pair; the key must be an already existing Twig file located in the templates folder for the currently active theme.<br />
        <i>Example: hs2-quilt-default|Default Layout</i><br />Leave empty to reset to defaults.'),
      '#default_value' => !empty($config->get('layouts')) ? $config->get('layouts') : $this->defaultQuiltLayouts,
    );

    // Determine available content types and create an array of key|value pairs
    // for listing them.
    $valid_bundles = array();
    $default_bundles = array();
    $types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    foreach ($types as $key => $value) {
      $valid_bundles[$key] = ucwords($key);
      $default_bundles[$key] = 0;
    }

    $form['entities'] = array(
      '#type' => 'checkboxes',
      '#options' => $valid_bundles,
      '#title' => $this->t('Use Content Types as Atoms'),
      '#description' => $this->t('Select any Content Types that should be used as valid sources for atoms in the quilt.<br />
        <strong>Be sure to override the ProcessAtom service with a custom module in order to introduce field-specific processing of these bundles.</strong>'),
      '#default_value' => !empty($config->get('entities')) ? $config->get('entities') : $default_bundles,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $layout_string = $form_state->getValue('layouts');

    // Reset to defaults if field is left blank on form submit.
    if (empty($layout_string)) {
      $form_state->setValue('layouts', $this->defaultQuiltLayouts);
    }
    // Validate that all lines are key|option pairs, and that corresponding Twig
    // templates exist for each key.
    else {
      $module_templates = DRUPAL_ROOT . '/' . drupal_get_path('module', 'hs2_blocks_quilt_component') . '/templates/';
      $theme_templates = DRUPAL_ROOT . '/' . \Drupal::service('theme.manager')->getActiveTheme()->getPath() . '/templates/';
      $layouts_arr = explode(PHP_EOL, $layout_string);

      foreach($layouts_arr as $item) {
        $entry = explode('|', $item);
        if (count($entry) == 2 && !empty($entry[0]) && !empty($entry[1])) {
          $template_filename = str_replace('_', '-', $entry[0]) . '.html.twig';
          if (!file_exists($theme_templates . $template_filename)) {
            if (!file_exists($module_templates . $template_filename)) {
              $form_state->setErrorByName('layouts', $this->t('The Twig template ' . $template_filename . ' was not found in the templates folder for the current active theme.'));
              break;
            }
          }
          $valid_layouts[trim($entry[0])] = trim($entry[1]);
        }
        else {
          $form_state->setErrorByName('layouts', $this->t('All layout options must be entered as key|value pairs.'));
          break;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('hs2_blocks_quilt_component.settings')
      ->set('layouts', $form_state->getValue('layouts'))
      ->set('entities', $form_state->getValue('entities'))
      ->save();
  }
}