<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component\ProcessAtomInterface.
 */

namespace Drupal\hs2_blocks_quilt_component;

interface ProcessAtomInterface {

  /** Retrieve field values according to the type of atom containing the
   * data.
   *
   * @param object $paragraph
   *  The entity containing the atom values.
   * @param string $layout
   *  The layout option select for the quilt side containing the atom.
   * @param integer $delta
   *  The order of the element in the quit side.
   *
   * @return array()
   *  A render array for displaying the atom.
   */
  public function buildAtomValues($paragraph, $layout, $delta);

  /**
   * Extract field data from the default atom sources - reusable atom blocks and
   * custom content provided within the paragraph.
   *
   * @param string $atom_type
   *  The machine name of the entity bundle.
   */
  public function processDefaultAtom($atom_type);

  /**
   * Extract field data from an entity other than the default atom sources.
   *
   * @param string $atom_type
   *  The machine name of the entity bundle.
   */
  public function processCustomAtom($atom_type);

  /**
   * Retrieve an image to be used as a quilt atom background, or a transparent
   * shim to force sizing if no image was provided.
   *
   * @param int $fid
   *  The file id of the image to be used.
   * @param string $layout
   *  The machine name of the layout selected for the quilt side.
   * @param int $delta
   *  The position of the atom in the paragraph.
   *
   * @return array
   *  An image render array.
   */
  public function getBgImg($fid, $layout, $delta);
}