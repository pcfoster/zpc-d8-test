<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component\ProcessStylesInterface.
 */

namespace Drupal\hs2_blocks_quilt_component;

interface ProcessStylesInterface {

  /**
   * Concatenate all class names that need to be added to an atom.
   *
   * @param $paragraph
   *  The paragraph entity containing the atom.
   */
  public function buildAtomClasses($paragraph);
}