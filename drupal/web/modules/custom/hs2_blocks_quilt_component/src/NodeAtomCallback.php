<?php

/**
 * @file
 * Contains Drupal\hs2_blocks_quilt_component\NodeAtomCallback
 */

namespace Drupal\hs2_blocks_quilt_component;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormStateInterface;

class NodeAtomCallback {

  /**
   * Ajax callback to return the subform for a side of the Quilt wizard.
   */
  public function setBundle(array &$form, FormStateInterface $form_state) {

    $response = new AjaxResponse();

    // Determine which element triggered the Ajax callback and return the
    // subform corresponding to the that it belongs to.
    if ($trigger = $form_state->getTriggeringElement()) {
      $parents = $trigger['#field_parents'];
      foreach ($parents as $parent) {
        if (strstr($parent, '_side')) {
          $quilt_side = $parent;
          break;
        }
      }

      if ($quilt_side) {
        $side_class = str_replace('_', '-', $quilt_side);
        $response->addCommand(new HtmlCommand('#edit-' . $side_class . '-wrapper', $form[$quilt_side]));
      }
    }

    return $response;
  }
}