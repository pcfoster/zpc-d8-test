<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component\ProcessAtom.
 */

namespace Drupal\hs2_blocks_quilt_component;

use Drupal\file\Entity\File;

class ProcessAtom implements ProcessAtomInterface {

  /**
   * The image style processing service.
   *
   * @var \Drupal\hs2_blocks_quilt_component\GetImageStyle
   */
  private $imageStyleGetter;

  /**
   * The paragraph entity containing the atom.
   */
  public $paragraph;

  /**
   * The machine name of the layout selected for the quilt side containing the
   * paragraph.
   */
  public $layout;

  /**
   * The index of the item within the paragraph entity.
   */
  public $delta;

  /**
   * The normalized atom values that will be returned to the custom field
   * formatter.
   */
  public $atomValues;

  /**
   * Constructs a ProcessAtom instance.
   */
  public function __construct($get_image_style_service) {
    $this->imageStyleGetter = $get_image_style_service;
  }

  /**
   * {@inheritdoc}
   */
  public function buildAtomValues($paragraph, $layout, $delta) {
    $this->paragraph = $paragraph;
    $this->layout = $layout;
    $this->delta = $delta;
    $this->atomValues = array();

    if ($atom_type = $this->paragraph->field_hs2_quilt_atom_type->value) {
      // Build entity uuid (used as css id).
      $this->atomValues['uuid'] = 'qs-' . $this->paragraph->uuid();

      // Processing for default atom sources provided by the quilt module.
      if ($atom_type == 'block' || $atom_type == 'custom') {
        $this->processDefaultAtom($atom_type);
      }
      // Processing for custom/site-specific atom sources.
      else {
        $this->processCustomAtom($atom_type);
      }
    }

    return $this->atomValues;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefaultAtom($atom_type) {
    switch ($atom_type) {
      // If a reusable atom, load the entity and get the values we need.
      case 'block':
        $bid = $this->paragraph->field_hs2_quilt_atom_ref->target_id;
        if ($block = \Drupal\block_content\Entity\BlockContent::load($bid)) {
          $this->atomValues['link'] = $block->field_hs2_atom_link->first()->getUrl();
          $this->atomValues['bg_img'] = $this->getBgImg($block->field_hs2_atom_background->target_id, $this->layout, $this->delta);
          $this->atomValues['headline'] = $block->field_hs2_atom_title->value;
          $this->atomValues['byline'] = $block->field_hs2_atom_byline->value;
          $this->atomValues['cta'] = $block->field_hs2_atom_cta_text->value;
        }
        break;
      // If custom content get field values directly from the paragraph entity.
      case
      'custom':
        $this->atomValues['link'] = $this->paragraph->field_hs2_quilt_atom_link->first()->getUrl();
        $this->atomValues['bg_img'] = $this->getBgImg($this->paragraph->field_hs2_quilt_atom_bg_img->target_id, $this->layout, $this->delta);
        $this->atomValues['headline'] = $this->paragraph->field_hs2_quilt_atom_headline->value;
        $this->atomValues['byline'] = $this->paragraph->field_hs2_quilt_atom_byline->value;
        $this->atomValues['cta'] = $this->paragraph->field_hs2_quilt_atom_cta_text->value;
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processCustomAtom($atom_type) {
    switch ($atom_type) {
      default:
        // If a node was selected as the atom but no bundle-specific
        // processing has been defined, pull the title and link from
        // the referenced node as basic content for display.
        if ($nid = $this->paragraph->field_hs2_quilt_atom_node->target_id) {
          if ($node = \Drupal\node\Entity\Node::load($nid)) {
            $nid = $node->nid->value;
            $url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $nid]);
            $url = $url->toString();
            $this->atomValues['link'] = $url;
            $this->atomValues['bg_img'] = $this->getBgImg(NULL, $this->layout, $this->delta);
            $this->atomValues['headline'] = $node->getTitle();
            $this->atomValues['cta'] = 'See More';
          }
        }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBgImg($fid, $layout, $delta) {
    // Provide an image (or shim) in a specific format if a style is available.
    $default_styles = array('large', 'medium', 'thumbnail');
    $style_name = $this->imageStyleGetter->getAtomImageStyle($layout, $delta);

    if ($file = File::load($fid)) {
      $uri = $file->getFileUri();
      $img_render_array = [
        '#theme' => 'image_style',
        '#style_name' => $style_name,
        '#uri' => $uri,
      ];
    }
    // If the file is not found or $fid is NULL use a shim with the same
    // aspect ratio as the image style.
    else {
      if (!in_array($style_name, $default_styles)) {
        $img_render_array = [
          '#theme' => 'image',
          '#uri' => drupal_get_path('module', 'hs2_blocks_quilt_component') . '/img/shims/' . $style_name . '_shim.png',
        ];
      }
      // If no image style is available use a generic 1x1 px shim.
      else {
        $img_render_array = [
          '#theme' => 'image',
          '#uri' => drupal_get_path('module', 'hs2_blocks_quilt_component') . '/img/shim.png',
        ];
      }
    }

    return $img_render_array;
  }
}