<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component\GetImageStyle.
 */

namespace Drupal\hs2_blocks_quilt_component;

class GetImageStyle implements GetImageStyleInterface {

  /**
   * {@inheritdoc}
   */
  public function getAtomImageStyle($layout, $delta) {
    $image_style = 'thumbnail';

    switch($layout) {

      case 'hs2_quilt_one_2x2' :
        $image_style = 'hs2_quilt_2x2';
        break;

      case 'hs2_quilt_two_2x1' :
        $image_style = 'hs2_quilt_2x1';
        break;

      case 'hs2_quilt_two_1x2' :
        $image_style = 'hs2_quilt_1x2';
        break;

      case 'hs2_quilt_four_1x1' :
        $image_style = 'hs2_quilt_1x1';
        break;

      case 'hs2_quilt_two_1x1_one_2x1' :
        if ($delta <= 1) {
          $image_style = 'hs2_quilt_1x1';
        }
        else {
          $image_style = 'hs2_quilt_2x1';
        }
        break;

      case 'hs2_quilt_one_2x1_two_1x1' :
        if ($delta == 0) {
          $image_style = 'hs2_quilt_2x1';
        }
        else {
          $image_style = 'hs2_quilt_1x1';
        }
        break;
    }

    return $image_style;
  }
}