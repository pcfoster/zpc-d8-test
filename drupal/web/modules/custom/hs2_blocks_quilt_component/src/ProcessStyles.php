<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component\ProcessStyles.
 */

namespace Drupal\hs2_blocks_quilt_component;

use Drupal\Component\Utility\Html;

class ProcessStyles implements ProcessStylesInterface {

  /**
   * {@inheritdoc}
   */
  public function buildAtomClasses($paragraph) {
    $classes = '';

    // Set a class to indicate which type of atom is being rendered.
    $classes .= ' quiltrow__type--' . $paragraph->field_hs2_quilt_atom_type->value;

    // Set a background class on the atom.
    if ($paragraph->field_hs2_quilt_atom_bg_color->value) {
      $classes .= ' ' . $paragraph->field_hs2_quilt_atom_bg_color->value;
    }

    // Set text-reverse class on the atom if white was chosen as the text color.
    if ($paragraph->field_hs2_quilt_atom_text_color->value) {
      $classes .= ' ' . $paragraph->field_hs2_quilt_atom_text_color->value;
    }

    // Set CTA button color class on the atom.
    if ($paragraph->field_hs2_quilt_atom_cta_color->value) {
      $classes .= ' ' . $paragraph->field_hs2_quilt_atom_cta_color->value;
    }

    // Set CTA button size class on the atom.
    if ($paragraph->field_hs2_quilt_atom_cta_size->value) {
      $classes .= ' ' . $paragraph->field_hs2_quilt_atom_cta_size->value;
    }

    // Set text (headline/byline) position in the atom.
    if ($paragraph->field_hs2_quilt_atom_text_pos->value) {
      $classes .= ' ' . $paragraph->field_hs2_quilt_atom_text_pos->value;
    }

    // Set tablet-text class on the atom if the text should drop on tablets.
    if ($paragraph->field_hs2_quilt_atom_drop_tablet->value && $paragraph->field_hs2_quilt_atom_drop_tablet->value == 1) {
      $classes .= ' quiltrow__hide--tablet-txt';
    }

    // Set mobile-text class on the atom if the text should drop on mobile.
    if ($paragraph->field_hs2_quilt_atom_drop_mobile->value && $paragraph->field_hs2_quilt_atom_drop_mobile->value == 1) {
      $classes .= ' quiltrow__hide--mobile-txt';
    }

    // Add custom user-provided classes the atom.
    if ($paragraph->field_hs2_quilt_atom_classes->value) {
      $classes .= ' ' . Html::escape($paragraph->field_hs2_quilt_atom_classes->value);
    }

    return $classes;
  }
}