<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Implements hook_preprocess_form_element().
 *
 * Add classes to allow for addition of layout icons to quilt builder/wizard.
 */
function hs2_blocks_quilt_component_preprocess_form_element(&$variables) {
  $row_sides = array('field_hs2_quilt_rows_lt_layout', 'field_hs2_quilt_rows_rt_layout');
  if (isset($variables['name']) && in_array($variables['name'], $row_sides)) {
    $name = str_replace('_', '-', $variables['element']['#attributes']['value']);
    $variables['attributes']['class'][] = 'quiltrow__layout--' . $name;
  }
}

/**
 * Implements hook_form_alter().
 *
 * Additional processing and functionality for quilt builder/wizard.
 */
function hs2_blocks_quilt_component_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Look for the Block Edit form for Quilt Rows.
  if ($form_id == 'block_content_quilt_rows_edit_form' || $form_id == 'block_content_quilt_rows_form') {
    // Attach Admin Library (CSS & JS)
    $form['#attached']['library'][] = 'hs2_blocks_quilt_component/hs2_blocks_quilt_component_admin';

    // Alter the allowed target bundles for the node atom entity reference
    // autocomplete field based on the valid bundles that appear in the atom type
    // dropdown.
    $row_sides = array('field_hs2_quilt_rows_lt_side', 'field_hs2_quilt_rows_rt_side');
    $invalid_options = array('_none', 'block', 'custom');

    foreach ($row_sides as $side) {
      $side_class = str_replace('_', '-', $side);
      $item = 0;
      while ($item <= 3) {
        $str_item = (string) $item;
        if (isset($form[$side]['widget'][$str_item])) {

          // Add Ajax callback to Atom Type dropdown that will set the allowed
          // bundles for the Atom Node Entity reference field based on the value
          // that was selected in the dropdown.
          $form[$side]['widget'][$str_item]['subform']['field_hs2_quilt_atom_type']['widget']['#ajax'] = array(
            'callback' => 'Drupal\hs2_blocks_quilt_component\NodeAtomCallback::setBundle',
            'wrapper' => '#edit-' . $side_class,
            'effect' => 'fade',
            'event' => 'change',
            'progress' => array(
              'type' => 'throbber',
              'message' => NULL,
            ),
          );

          // Assemble an array of all possible values, excluding empty (none)
          // as well as the two defaults provided by the module - reusable atom
          // and custom content.
          $atom_type_options = $form[$side]['widget'][$str_item]['subform']['field_hs2_quilt_atom_type']['widget']['#options'];
          foreach ($atom_type_options as $key => $value) {
            if (in_array($key, $invalid_options)) {
              unset($atom_type_options[$key]);
            }
          }
          $atom_type_keys = array_keys($atom_type_options);

          // Remove any bundles that aren't currently enabled in the Quilt
          // Configuration settings.
          $node_atom_targets = $form[$side]['widget'][$str_item]['subform']['field_hs2_quilt_atom_node']['widget']['0']['target_id']['#selection_settings']['target_bundles'];
          foreach ($node_atom_targets as $key => $value) {
            if (!in_array($key, $atom_type_keys)) {
              unset($node_atom_targets[$key]);
            }
          }

          // Limit target bundles for entityreference to node atoms if an atom
          // type has already been selected.
          if ($state_side = $form_state->getValue($side)) {
            if (isset($state_side[$str_item]['subform']['field_hs2_quilt_atom_type']['0'])) {
              $atom_type = $state_side[$str_item]['subform']['field_hs2_quilt_atom_type']['0']['value'];
              foreach ($node_atom_targets as $bundle_machine => $bundle_label) {
                if ($bundle_machine != $atom_type) {
                  unset($node_atom_targets[$bundle_machine]);
                }
              }
            }
          }

          $form[$side]['widget'][$str_item]['subform']['field_hs2_quilt_atom_node']['widget']['0']['target_id']['#selection_settings']['target_bundles'] = $node_atom_targets;
        }
        $item++;
      }
    }


  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * Provide a type-specific template suggestion for all blocks with a bundle of
 * quilt_rows.
 */
function hs2_blocks_quilt_component_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  if (isset($variables['elements']['content']['#block_content']) && $variables['elements']['content']['#block_content']->bundle() == 'quilt_rows') {
    $suggestions[] = 'block__' . $variables['elements']['content']['#block_content']->bundle();
  }
}

/**
 * Implements hook_theme_registry_alter().
 *
 * Provide a path and template to force quilt_rows blocks to default to the Twig
 * template provided by the custom module.
 */
function hs2_blocks_quilt_component_theme_registry_alter(&$theme_registry) {
  // Copy all settings for default block provided by Drupal.
  $theme_registry['block__quilt_rows'] = $theme_registry['block'];
  // Update path and template.
  $theme_registry['block__quilt_rows']['template'] = 'block--quilt-rows';
  $theme_registry['block__quilt_rows']['path'] = drupal_get_path('module', 'hs2_blocks_quilt_component') . '/templates';
}


/**
 * Implements hook_theme().
 */
function hs2_blocks_quilt_component_theme() {
  // Define base/fallback Twig template for quilt side layout.
  $theme_funcs = array(
    'hs2_quilt_default' => array(
      'variables' => array(
        'content' => NULL,
        'article_classes' => NULL,
      ),
    ),
  );

  // Process user-defined layouts stored in config var set through admin
  // interface to register site-specific Twig templates.
  $valid_layouts = hs2_blocks_quilt_component_get_valid_layouts();
  foreach($valid_layouts as $key => $value) {
    $theme_funcs[$key] = array(
      'variables' => array(
        'content' => NULL,
        'article_classes' => NULL,
      ),
    );
  }

  return $theme_funcs;
}

/**
 * Set dynamic allowed values for the layout options.
 *
 * @param \Drupal\field\Entity\FieldStorageConfig $definition
 *   The field definition.
 * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
 *   The entity being created if applicable.
 * @param bool $cacheable
 *   Boolean indicating if the results are cacheable.
 *
 * @return array
 *   An array of possible key and value options.
 *
 * @see options_allowed_values()
 */
function hs2_blocks_quilt_components_layout_allowed_values(FieldStorageConfig $definition, ContentEntityInterface $entity = NULL, $cacheable) {
  $valid_layouts = hs2_blocks_quilt_component_get_valid_layouts();

  return $valid_layouts;
}

/**
 * Set dynamic allowed values for nodes used as atoms.
 *
 * @param \Drupal\field\Entity\FieldStorageConfig $definition
 *   The field definition.
 * @param \Drupal\Core\Entity\ContentEntityInterface|null $entity
 *   The entity being created if applicable.
 * @param bool $cacheable
 *   Boolean indicating if the results are cacheable.
 *
 * @return array
 *   An array of possible key and value options.
 *
 * @see options_allowed_values()
 */
function hs2_blocks_quilt_components_node_atom_allowed_values(FieldStorageConfig $definition, ContentEntityInterface $entity = NULL, $cacheable) {
  $valid_atoms = array(
    'block' => 'Reusable Atom',
    'custom' => 'Custom Content',
  );
  $selected_atoms = \Drupal::config('hs2_blocks_quilt_component.settings')->get('entities');
  foreach ($selected_atoms as $bundle => $enabled) {
    if ($enabled !== 0) {
      $valid_atoms[$bundle] = ucwords($bundle);
    }
  }

  return $valid_atoms;
}

/**
 * Retrive saved quilt layout config var and process, retuning all valid layout
 * options as an array.
 *
 * @return array
 *   An array of possible layout options. Twig template names are used as keys
 *   and human-readable labels are values.
 */
function hs2_blocks_quilt_component_get_valid_layouts() {
  $valid_layouts = array();
  $layout_string = \Drupal::config('hs2_blocks_quilt_component.settings')->get('layouts');

  $layouts_arr = explode(PHP_EOL, $layout_string);
  foreach($layouts_arr as $item) {
    $entry = explode('|', $item);
    if (count($entry) == 2) {
      $valid_layouts[trim($entry[0])] = trim($entry[1]);
    }
  }

  return $valid_layouts;
}