*Instructions*

Make a copy of the Block Quilt Rows Twig Template (`modules/custom/hs2_blocks_quilt_component/templates/block--quilt-rows.html.twig`) and place it within your site's theme template folder.

```
<div class="quiltrow__block">

  <div{{ attributes }}>

    {% block content %}

      {{ content }}

    {% endblock %}

  </div>

</div>
```

There is a SASS component to this module to provide basic theming so it can be used immediately out of the box.  In order to update the styles for this module directly, one must have the following ruby gems installed.

* gem "sass", "~>3.3"
* gem "compass", "~>1.0"
* gem "singularitygs", "~>1.4"

To compile sass, run `compass watch` via commandline from the root of this module.