/**
 * @file
 * Custom Javascript for Wilson Quilt Rows.
 * @TODO: Update D7 version of the code below to D8.
 */
(function ($) {
//   /**
//    * Define base namespace.
//    */
  if (!Drupal.quiltRowAdmin) {
    Drupal.quiltRowAdmin = {};
  }

  Drupal.behaviors.quiltRowAdmin = {
    attach: function (context, settings) {

      var sides = ['lt', 'rt'];
      for (var side in sides) {

        // Prevent reordering of items in the field collections in this context.
        $('a.tabledrag-handle').remove();

        // Removing "remove" button from paragraph items.  Rows should not be deleted.
        $('.paragraphs-dropbutton-wrapper').remove();

        // Start by hiding all (4) items in the field collection on initial load.
        // field-hs2-quilt-rows-lt-side-values
        $('[id*="field-hs2-quilt-rows-'+sides[side]+'-side-values"] tr').hide();

        // Show only the row(s) needed for the previously selected layout
        // (if one was already chosen).
        current_layout = $("input[name$='field_hs2_quilt_rows_"+sides[side]+"_layout']:checked").val();
        if (typeof current_layout != 'undefined') {
          toggleQuiltFCRows(current_layout, sides[side]);
        }

        // Toggle which FC rows are displayed to the user when they initially
        // select or switch between layouts.
        $("input[name$='field_hs2_quilt_rows_"+sides[side]+"_layout']").once().on("change", function() {
          // First determine which side of the quilt row is being updated.
          var side_classes = $(this).parents("details.quiltrow__side").attr("class");
          var select_side = (side_classes.indexOf("quiltrow__side--left") >= 0 ? "lt" : "rt");

          // Hide all rows in that side as a starting point.
          $('[id*="field-hs2-quilt-rows-'+select_side+'-side-values"] tr').hide();
          var layout = $(this).val();

          // Then show only the ones that are required.
          toggleQuiltFCRows(layout, select_side);
        });

        var all_fields = [
          'field--name-field-hs2-quilt-atom-ref',
          'field--name-field-hs2-quilt-atom-headline',
          'field--name-field-hs2-quilt-atom-byline',
          'quiltrow__wrapper--custom',
          'field--name-field-hs2-quilt-atom-cta-text',
          'field--name-field-hs2-quilt-atom-node'
        ];
        var atomTypeField = $('td .field--name-field-hs2-quilt-atom-type select.form-select');

        // Hide all type-specific fields in each row of the FC on initial load.
        $.each(all_fields, function(index,value) {
          $('.'+value).children().addClass('quiltrow--hidden').hide();
        });

        // Show only the row(s) required for the previously selected atom type
        // (if one was already chosen).
        $.each(atomTypeField, function() {
          var atom_type = $(this).val();
          //console.log(this);
          if (atom_type != '_none') {
            var reference = $(this).parents('.field--name-field-hs2-quilt-atom-type').attr('id');
            $('#' + reference).parents('td').removeClass().addClass('quiltrow__atom-type--'+atom_type);
            toggleQuiltFCInputs(atom_type, reference);
          }
        });

        // When an Atom type is chosen from a select update the display to only
        // show those fields relevant to that atom type.
        atomTypeField.on( "change", function() {
          // console.log(this);
          var select_parent = $(this).parents('td');
          var reference = $(this).parents('.field--name-field-hs2-quilt-atom-type').attr('id');
          // Hide all fields initially.
          $.each(all_fields, function(index,value) {
            $('#' + reference).siblings('.'+value).children().hide();
          });

          // Show valid inputs.
          var atom_type = $(this).val();
          if (atom_type != '_none') {
            var reference = $(this).parents('.field--name-field-hs2-quilt-atom-type').attr('id');
            $('#' + reference).parents('td').removeClass().addClass('quiltrow__atom-type--'+atom_type);
            toggleQuiltFCInputs(atom_type, reference);
          }
        });
      }

      // Bind a click event to the form submit button to clear the values from
      // all items in the field collection that won't be used as an Atom.
      // For example if the user selects Two 2x1 and provides values for both
      // atoms, then switches to One 2x2 before saving, we clear the second set
      // of unused values.
      // This prevents the storage of unncessary values in the database.
      $('button.form-submit').once().click(function() {
        var left_layout = $("input[name$='field_hs2_quilt_rows_lt_layout']:checked").val();
        presaveProcess(left_layout, 'left');
        var right_layout = $("input[name$='field_hs2_quilt_rows_rt_layout']:checked").val();
        presaveProcess(right_layout, 'right');
      });
    }
  };

  // Selectively reveals one or more individual field collection items, each
  // corresponding to an Atom.
  function toggleQuiltFCRows(layout, side) {
    $('[id*="field-hs2-quilt-rows-'+side+'-side-values"] tr').removeClass();

    switch (layout) {

      case "hs2_quilt_one_2x2":
        var rows = [1];
        break;

      case "hs2_quilt_two_2x1":
        var rows = [1,2];
        break;

      case "hs2_quilt_two_1x2":
        var rows = [1,2];
        break;

      case "hs2_quilt_four_1x1":
        var rows = [1,2,3,4];
        break;

      case "hs2_quilt_two_1x1_one_2x1":
        var rows = [1,2,3];
        break;

      case "hs2_quilt_one_2x1_two_1x1":
        var rows = [1,2,3];
        break;
    }

    showQuiltFCRows(rows, side, layout);
  }

  // Reveal one or more rows in a field collection, appending a required layout
  // class for additional targeting.
  function showQuiltFCRows(rows, side, layout) {
    $.each(rows, function(index,value) {
      $('[id*="field-hs2-quilt-rows-'+side+'-side-values"] tr:nth-child('+value+')').fadeIn().addClass("quiltrow__layout--"+layout);
    });
  }

  // Selectively reveals one or more fields relevant to a specific Atom type.
  function toggleQuiltFCInputs(type, reference) {
    switch (type) {

      case "block":
        var fields = [
          'field--name-field-hs2-quilt-atom-ref'
        ];
        break;

      case "custom":
        var fields = [
          'field--name-field-hs2-quilt-atom-headline',
          'field--name-field-hs2-quilt-atom-byline',
          'quiltrow__wrapper--custom',
          'field--name-field-hs2-quilt-atom-cta-text'
        ];
        break;
      case "article":
      case "basic_content_type":
      case "locked_content_type":
      case "page":
        var fields = [
          'field--name-field-hs2-quilt-atom-node'
        ];
    }

    showQuiltFCInputs(fields, reference);
  }

  // Reveal one or more inputs in a FC row.
  function showQuiltFCInputs(fields, reference) {
    $.each(fields, function(index,value) {
      $('#' + reference).siblings('.'+value).children().fadeIn();
    });
  }

  // Utility function to drop unused values on one side of the Quilt Row
  // according to the layout chosen for that side.
  function presaveProcess(layout, side) {
    switch (layout) {
      case "hs2_quilt_one_2x2":
        clearUnusedAtoms([2,3,4], side);
        break;

      case "hs2_quilt_two_2x1":
        clearUnusedAtoms([3,4], side);
        break;

      case "hs2_quilt_two_1x2":
        clearUnusedAtoms([3,4], side);
        break;

      case "hs2_quilt_four_1x1":
        break;

      case "hs2_quilt_two_1x1_one_2x1":
        clearUnusedAtoms([4], side);
        break;

      case "hs2_quilt_one_2x1_two_1x1":
        clearUnusedAtoms([4], side);
        break;
    }
  }

  // Clears all form fields in each row representing an atom that isn't used
  // in the selected layout.
  function clearUnusedAtoms(indices, side) {
    for (var i in indices) {
      $("#field-hs2-quilt-rows-"+side+"-side-values tr:nth-child("+indices[i]+") input").val('');
      $("#field-hs2-quilt-rows-"+side+"-side-values tr:nth-child("+indices[i]+") select").val('_none');
      $("#field-hs2-quilt-rows-"+side+"-side-values tr:nth-child("+indices[i]+") input[type=checkbox]").prop('checked', false);
    }
  }

})(jQuery);