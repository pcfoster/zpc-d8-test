(function ($) {

  Drupal.behaviors.quiltRowTheme = {
    attach: function (context, settings) {

      // Remove any empty paragraph row blocks.
      if ($('.quiltrow__block--pane').hasClass('quiltrow__type--')) {
        $('.quiltrow__type--').remove();
      }
      // Adding a special class to Shim images.
      if ($('.quiltrow__atom--img img[src*="shim.png"]')) {
        $('.quiltrow__atom--img img[src*="shim.png"]').parent().addClass('img--shim');
      }

    }
  };

}(jQuery));