<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component_override_examples\GetImageStyleOverride.
 */

namespace Drupal\hs2_blocks_quilt_component_override_examples;

use Drupal\hs2_blocks_quilt_component\GetImageStyle;

class GetImageStyleOverride extends GetImageStyle {

  /**
   * The parent GetImageStyle service.
   */
  private $innerService;

  public function __construct(GetImageStyle $inner_service) {
    $this->innerService = $inner_service;
  }

  /**
   * {@inheritdoc}
   */
  public function getAtomImageStyle($layout, $delta) {
    $image_style = 'thumbnail';

    return $image_style;
  }
}