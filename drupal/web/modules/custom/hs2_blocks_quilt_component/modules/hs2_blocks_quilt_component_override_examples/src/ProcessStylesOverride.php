<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component_override_examples\ProcessStylesOverride.
 */

namespace Drupal\hs2_blocks_quilt_component_override_examples;

use Drupal\hs2_blocks_quilt_component\ProcessStyles;

class ProcessStylesOverride extends ProcessStyles {

  /**
   * The parent ProcessStyles service.
   */
  private $innerService;

  public function __construct(ProcessStyles $inner_service) {
    $this->innerService = $inner_service;
  }

  /**
   * {@inheritdoc}
   */
  public function buildAtomClasses($paragraph) {
    $classes = '';

    return $classes;
  }
}