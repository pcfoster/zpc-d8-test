<?php

/**
 * @file
 * Contains \Drupal\hs2_blocks_quilt_component_override_examples\ProcessAtomOverride.
 */

namespace Drupal\hs2_blocks_quilt_component_override_examples;

use Drupal\hs2_blocks_quilt_component\ProcessAtom;

class ProcessAtomOverride extends ProcessAtom {

  /**
   * The parent ProcessAtom service.
   */
  private $innerService;

  public function __construct(ProcessAtom $inner_service, $get_image_style_service) {
    $this->innerService = $inner_service;
    parent::__construct($get_image_style_service);
  }

  /**
   * {@inheritdoc}
   */
  public function processCustomAtom($atom_type) {
    switch ($atom_type) {

      default:
        // If a node was selected as the atom but no bundle-specific
        // processing has been defined above, pull the title and link from
        // the referenced node as basic content for display.
        if ($nid = $this->paragraph->field_hs2_quilt_atom_node->target_id) {
          if ($node = \Drupal\node\Entity\Node::load($nid)) {
            $nid = $node->nid->value;
            $url = \Drupal\Core\Url::fromRoute('entity.node.canonical', ['node' => $nid]);
            $url = $url->toString();
            $this->atomValues['link'] = $url;
            $this->atomValues['bg_img'] = $this->innerService->getBgImg(NULL, $this->layout, $this->delta);
            $this->atomValues['headline'] = $node->getTitle();
            $this->atomValues['headline'] = 'This atom is overridden';
            $this->atomValues['cta'] = 'See More';
          }
        }
    }
  }
}