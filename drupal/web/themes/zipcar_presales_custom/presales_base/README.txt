DEVELOPING WITH A FOUNDATION SUBTHEME with SHARED PATTERNS and PATTERN LAB styleguide
----------------------------------------

Component-based FED is the new hotness.
* Let's build our components using Atomic principles!
* Let's prototype new components for quick review and USE THE PROTOTYPE CODE in production, keeping us DRY
* Let's deliver that prototype tool as a living style guide to keep future development in style

Foundation / Patternlab Setup
----------------------------------------

This sub_theme was generated using `drush fst` command.  On your first visit, the node_modules directory
of dependent files will need to be generated, as it is gitignored and not part of the repo.

  1. vagrant ssh into the VM
  2. cd to this theme's root directory
  3. `sudo apt install npm`, `sudo apt install nodejs-legacy`, `npm install`.

You should now have access to the gulp task runner.  Try running `gulp`. You may see errors from
Patternlab not being properly set up yet; let's address that now.

  1. cd to pattern-lab
  2. `composer install` will generate a vendor directory that will be gitignored

You should now be able to see the Pattern Lab style guide at
[your-local-drupal-project]/themes/[this-theme-path]/pattern-lab/public/index.html

Gulp tasks
----------------------------------------

From the theme root directory, you can run `gulp` or `gulp build` to recompile all Drupal sass and
rebuild the Pattern Lab files.  You can run individual tasks or set a watcher for sass only, patterns, or both.
See the this_theme/gulpfile.js for details.
Note that you can enable or disable gulp's running drush clear cache in the this_theme/config.js file,
depending on your workflow.

The MAGIC SCSS FILE
----------------------------------------

In this theme scss directory, you will see Zurb Foundation's idea of how to structure sass files:
base, layout, module directories, etc. It's not a bad idea, but HS2 is feeling the atomic structure model.
So feel free to ignore those scss/ directories and the files inside.
_settings.scss contains a whole lot of default settings for Foundation.  You won't need to touch these either.

The [theme_name].scss file is where all the magic happens.  This file imports every other sass file and partial.
The compiled css for the drupal site (and the styleguide) come directly from this sass file.
So edit with care and confidence.

The zurb commenting in this file is pretty verbose, almost as chatty as this README; feel free to delete
or edit as you see fit.
  1. Leave the char setting as is.
  2. Continue to @import 'settings', 'foundation', 'motion-ui' and the motion transitions/animations.
  3. Leave $flex: false;  Your mileage may vary, but I haven't seen a project yet that hasn't suddenly exhibited
     some ill-defined weirdness that can only be solved by turning OFF this flex-grid.
  4. Comment out any imports from base, layouts, etc. unless there's a decent reason.

Now that you've emptied out main sass file, it's time to fill it up with references to our atomic components.

Build Components in Shared_Patterns Directory
----------------------------------------

Atoms!  Within shared_patterns/atoms,
  1. create a twig file or files with examples of your fonts, your paragraph and header elements, maybe color swatches.  (00-colors.twig)
  2. Create a scss file or files to style these atomic elements. (a-colors.scss)
  3. write a line that @imports that scss file in [theme_name]/scss/[theme_name].scss.

Run gulp build and visit the pattern-lab style guide in a browser.  You have entries in your style guide!
Visit your drupal root in a browser.  Create a test node so you can see your styling.  You have styling!

As you create molecules based on atoms, etc. you will be adding more @import lines into [theme_name].scss.
The order of those lines in the file will determine the order the scss will be compiled, so keep that in mind
if you need to do something fancy with inheritance.

Data in twig templates
----------------------------------------

A twig template in the shared_patterns directory will look something like this (m-exampleitem.twig):
  <div><h2>{{ header }}</h2><img src="{{ img_url }}" /><p>{{ content }}</p></div>.

This is the template for some content entity (a paragraph, let's say) with two text fields and an image.

Here's how we assign data to those {{ }} variables.  First, from Drupal.

  1. Create a drupal template in the [theme_name]/templates directory.  Use the twig debug tool to determine
  the proper naming convention for the entity; let's say e.g. it's paragraph--example-item.html.twig.
  (Note: in D8 you can create directories within the templates directory to keep things tidy)

  2. This drupal template file shouldn't have any markup in it.
    {% include "@molecules/paragraphs/m-exampleitem.twig"
      with {
      "header": content.field_header_text|field_value,
      "img_url": file_url(content.field_image_field|field_target_entity.uri.value|image_style('thumbnail')),
      "content": content.body
     }
    %}
    It will reference your shared_patterns/ twig template (in include) where all the layout actually happens.
    We use the "with" to pass the drupal content in the variables the twig template expects.

  It can be a little challenging to get the content structured properly.  Fortunately, we can look at core and
  other theme templates for clues, and there are some very handy twig filters in modules like
  * https://www.drupal.org/project/twig_extender
  * https://www.drupal.org/project/twig_field_values
  * https://www.drupal.org/project/twig_tweak

That's the difficult half.  For the prototype and/or style guide, we'll want to be able to look at this component
in pattern-lab and style it using some sort of dummy content.  We can keep the lorem ipsum right here in the
same shared_patterns/molecules/paragraphs/ directory as the twig template and sass file.

  1. Create a .json file named identically to the twig template.
  2. Create an object with the variable names as attributes:
    {
      "header": "Jabberwocky",
      "img_url": "/files/fpo.jpg",
      "content": "twas brillig and the slithy toves"
     }


THINGS TO KEEP IN MIND
----------------------------------------

  * The twig and sass files can be as granular as is sensible.
  * The integer in the twig names (under atom directory) are solely for sorting in the style guide.
  * Adding an "a-" or "m-" etc. in the file names is a good idea because you may see that file referenced later
  included in another template, or if you're using BEM for your DOM class structures.  The "a-" or "m-" will
  tell you where to start looking.


